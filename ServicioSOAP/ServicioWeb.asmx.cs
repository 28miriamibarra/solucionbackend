﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;

namespace ServicioSOAP
{
    public class ServicioWeb : System.Web.Services.WebService
    {
        [WebMethod]
        public bool Guardar(string Cliente, double Saldo)
        {
            try
            {
                var DLLG = new DLLGuardado.ClaseGuardado();
                if (DLLG.Guardar(Cliente, Saldo))
                    return true;
                else
                    return false;
            }
            catch (System.Exception ex)
            {
                return false;
            }
        }

        [WebMethod]
        public DataSet BuscarRegistro(int Folio)
        {
            var Conjunto = new DataSet();
            try
            {
                var DLLB = new DLLBuscarRegistros.ClaseBuscarRegistro();
                Conjunto = DLLB.BuscarRegistro(Folio);
                return Conjunto;
            }
            catch (System.Exception ex)
            {
                return Conjunto;
            }
        }

        [WebMethod]
        public bool Eliminar(int Folio)
        {
            try
            {
                var DLLE = new DLLEliminar.ClaseEliminarRegistro();
                if (DLLE.Eliminar(Folio))
                    return true;
                else
                    return false;
            }
            catch (System.Exception ex)
            {
                return false;
            }
        }

        [WebMethod]
        public bool Actualizar(int Folio, string Cliente, double Saldo)
        {
            try
            {
                var DLLA = new ClaseActualizar.ClaseActualizar();
                if (DLLA.Actualizar(Folio, Cliente, Saldo))
                    return true;
                else
                    return false;
            }
            catch (System.Exception ex)
            {
                return false;
            }
        }




    }
}
